PROJECT			:= Rafflesia
ROOT_PATH		:= 
PROJECT_PATH	:= $(ROOT_PATH)/$(PROJECT)
BUILD_PATH		:= $(PROJECT_PATH)/build
PREFIX          := /usr/local

RAFFLESIA_PATH	:= $(PROJECT_PATH)
RAFFLESIA_LIB	:= -L$(PREFIX)/lib -llua5.2 `pkg-config --libs glib-2.0` `pkg-config --libs msgpack` -pthread -lreadline -lnanomsg 

INCLUDE			= $(PREFIX)/include $(RAFFLESIA_PATH) /usr/include/lua5.2

CC				:= gcc
CFLAGS			:= -ggdb -Wall -Wextra -Werror -pedantic -std=c99 -Wno-long-long  -Wno-unused-parameter -Wno-unused-variable -Wno-unused-function
CFLAGS			+= $(patsubst %,-I%,$(INCLUDE))
CFLAGS			+= `pkg-config --cflags glib-2.0`
CFLAGS			+= `pkg-config --cflags msgpack`

RAFFLESIA_SRC		= $(RAFFLESIA_PATH)/rafflesia.c\
    				  $(RAFFLESIA_PATH)/main.c

RAFFLESIA_OBJ 		= $(patsubst %.c,%.o,$(filter %.c,$(RAFFLESIA_SRC)))

.PHONY : clean

rafflesia: $(RAFFLESIA_OBJ)
	$(CC) $(CFLAGS) -o $(BUILD_PATH)/$@ $(RAFFLESIA_OBJ) $(RAFFLESIA_LIB) ;

clean:
	rm $(RAFFLESIA_PATH)/*.d ; 
	rm $(RAFFLESIA_PATH)/*.o ;

%.d: %.c
	$(CC) -MM $(CFLAGS) $*.c > $@

-include $(RAFFLESIA_OBJ:.o=.d)

