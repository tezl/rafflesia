/*
 * Copyright © 2015 Izaak 
 *
 * */

#include <assert.h>
#include <glib.h>
#include <linux/kernel.h>
#include <lauxlib.h>
#include <lua.h>
#include <lualib.h>
#include <pthread.h>
#include <msgpack.h>
#include <nanomsg/nn.h>
#include <nanomsg/reqrep.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/sysinfo.h>
#include <time.h>

#include "rafflesia.h"

#define DPRINTF printf
#define PORT "7077"
#define SERVER "192.168.178.36"


static uint16_t S_master_port; /* TODO: FIXME: these are hacky... fix it */
static const char *S_master_address; 

static pthread_mutex_t S_master_lock;

struct t_thread_info {
    pthread_t thread_id;
    int thread_num;
    void *data;
};
typedef struct t_thread_info t_thread_info;

static void *ra_thr_client(void *);
static void *ra_thr_master_server(void *);
static void *ra_thr_slave_server(void *);
static char *ra_getline(void);
static int ra_send_data(int, const t_byte *, size_t);
static t_byte *ra_recv_data(int, size_t *);
static char *ra_make_message(const char *fmt, ...);
static char *ra_status_as_str(void);
static int ra_l_status(lua_State *L);
static t_message *ra_message_init_with_data(t_id, t_status_code,
                                            const t_byte *, size_t);
static t_message *ra_message_init(void);
static void ra_message_destroy(t_message *);
static void ra_message_destroy_with_data(t_message *);
static t_byte *ra_message_serialize(const t_message *, size_t *);
static t_message *ra_message_deserialize(const t_byte *, size_t);
static t_id ra_id_deserialize(const t_byte *, size_t);
static void *ra_thr_master_server(void *);
static void *ra_thr_slave_server(void *);
static void *ra_thr_client(void *);
static t_node *ra_node_init(void);
static t_byte *ra_node_serialize(const t_node *, size_t *);
static t_node *ra_node_deserialize(const t_byte *, size_t);
static void ra_node_server(t_node *, const char *);
static void ra_node_destroy(t_node *);
static void ra_set_master(const char*, uint16_t);

void ra_set_master(const char *master_address, uint16_t port)
{
    pthread_mutex_lock(&S_master_lock);
    S_master_server = master_address;
    S_master_port = port;
    pthread_mutex_unlock(&S_master_lock);
}

void *MY_malloc(size_t sz)
{
    void *rt = 0;

    rt = malloc(sz);
    if (rt == 0) {
        perror("* * * ERROR: could not allocate memory");
        return 0;
    }
    return rt;
}

void *MY_calloc(size_t count, size_t sz)
{
    void *rt = 0;

    rt = calloc(count, sz);
    if (rt == 0) {
        perror("* * * ERROR: could not allocate memory");
        return 0;
    }
    return rt;
}

void *MY_realloc(void *pt, size_t sz)
{
    void *rt = 0;

    rt = realloc(pt, sz);
    if (rt == 0) {
        perror("* * * ERROR: could not reallocate memory");
        MY_free(pt);
        return 0;
    }
    return rt;
}

void MY_free(void *pt)
{ 
    free(pt); pt = 0; 
}

char *ra_getline(void)
{
#define PROMPT "> "
    char *line_read = 0;
    line_read = readline(PROMPT);
    if (line_read && *line_read) {
        add_history(line_read);
    }
    return line_read;
#undef PROMPT
}

int ra_send_data(int sock, const t_byte *data, size_t data_sz)
{
    size_t sz = 0, nb = 0;

    sz = strlen(data);
    nb = nn_send(sock, data, data_sz, 0);
    if (sz != nb) {
        return -1;
    }
    return 0;
}

t_byte *ra_recv_data(int sock, size_t *out_sz)
{
    t_byte *buf = 0, *rt = 0;
    int nb = 0;
    
    nb = nn_recv(sock, &buf, NN_MSG, 0);
    if (nb > 0) {
        printf("RECEIVED \"%s\"\n", buf);
        rt = MY_malloc(sizeof(*rt) * nb);
        strncpy(rt, buf, nb);
        *out_sz = nb;
        nn_freemsg(buf);
        return rt;
    }
    return 0;
}

char *ra_make_message(const char *fmt, ...)
{
    /* see man page PRINTF(3) */
    int n = 0;
    int size = 128;
    char *p = 0, *np = 0;
    va_list ap;

    p = MY_malloc(size);
    while (1) {
        va_start(ap, fmt);
        n = vsnprintf(p, size, fmt, ap);
        va_end(ap);
        if (n < 0) {
            MY_free(p);
            return 0;
        }
        if (n < size) { return p; }
        /* realloc and keep looping until correct size is found */
        size = n + 1;
        np = MY_realloc(p, size);
        if (np == 0) { return 0; } 
        p = np;
   }
   return 0;
}

char *ra_status_as_str(void)
{
    time_t now;
    char buf[sizeof "2015-02-17T20:08:09Z"];
    struct sysinfo si;
    int rt = 0;

    rt = sysinfo(&si);
    if (rt != 0) {
        fprintf(stderr, "* * * ERROR: could not find system info\n");
        return 0;
    }
#define LOAD_AS_FLOAT(l)  ((float) (l)) / ((float) (1 << SI_LOAD_SHIFT))
#define B_TO_MB(x) ( ((float) (x)) / ((float) 1024 * 1024))
    time(&now);
    strftime(buf, sizeof buf, "%Y-%m-%dT%H:%M:%SZ", gmtime(&now));
    return ra_make_message("%s, uptime: %lds, load average: %.2f %.2f %.2f,"
                        " totalram: %.2fMB, freeram: %.2fMB",
                buf,
                si.uptime, 
                LOAD_AS_FLOAT(si.loads[0]),
                LOAD_AS_FLOAT(si.loads[1]),
                LOAD_AS_FLOAT(si.loads[2]),
                B_TO_MB(si.totalram),
                B_TO_MB(si.freeram));
#undef LOAD_AS_FLOAT
#undef B_TO_MB
}

int ra_l_status(lua_State *L)
{
    char *s = 0;

    s = ra_status_as_str();
    lua_pushstring(L, s);
    MY_free(s); s = 0;

    return 1;
}

t_message *ra_message_init_with_data(t_id id, t_status_code sc,
                                    const t_byte *data, size_t data_sz)
{
    t_message *msg = 0;

    msg = MY_malloc(sizeof(*msg));
    msg->node_id = id;
    msg->code = sc;
    msg->data = 0;
    msg->data_sz = 0;
    if (data_sz != 0) {
        msg->data = MY_malloc(sizeof(*(msg->data)) * data_sz);
        memcpy(msg->data, data, data_sz);
        msg->data_sz = data_sz;
    }
    return msg;
}

t_message *ra_message_init(void)
{
    t_message *msg = 0;

    msg = MY_malloc(sizeof(*msg));
    msg->node_id = 0;
    msg->code = 0;
    msg->data = 0;
    msg->data_sz = 0;
    return msg;
}

void ra_message_destroy(t_message *msg)
{
    MY_free(msg); msg = 0;
}

void ra_message_destroy_with_data(t_message *msg)
{
    MY_free(msg->data); msg->data = 0;
    MY_free(msg); msg = 0;
}

t_byte *ra_message_serialize(const t_message *msg, size_t *out_sz)
{
    t_byte *buf = 0;
    msgpack_sbuffer sbuf;
    msgpack_packer pck;

    msgpack_sbuffer_init(&sbuf);
    msgpack_packer_init(&pck, &sbuf, msgpack_sbuffer_write);
    msgpack_pack_array(&pck, 2);
    msgpack_pack_uint64(&pck, msg->node_id);
    msgpack_pack_uint32(&pck, msg->code);
    msgpack_pack_bin(&pck, msg->data_sz);
    msgpack_pack_bin_body(&pck, msg->data, msg->data_sz);
    *out_sz = sbuf.size;
    buf = MY_malloc(sizeof(*buf) * (*out_sz)); 
    memcpy(buf, sbuf.data, sbuf.size);
    msgpack_sbuffer_destroy(&sbuf);
    return buf;
}

t_message *ra_message_deserialize(const t_byte *data, size_t data_sz)
{
    t_message *out_msg = 0;
    msgpack_object obj;
    msgpack_unpacker pac;
    msgpack_unpacked result;
    
    out_msg = ra_message_init();
    msgpack_unpacker_init(&pac, MSGPACK_UNPACKER_INIT_BUFFER_SIZE);
    msgpack_unpacker_reserve_buffer(&pac, data_sz);
    memcpy(msgpack_unpacker_buffer(&pac), data, data_sz);
    msgpack_unpacker_buffer_consumed(&pac, data_sz);
    msgpack_unpacked_init(&result);
    while (msgpack_unpacker_next(&pac, &result)) {
        obj = result.data;
        switch (obj.type) {
            case MSGPACK_OBJECT_ARRAY:
                out_msg->node_id = obj.via.array.ptr[0].via.u64;
                out_msg->code = obj.via.array.ptr[1].via.u64;
                break;
            case MSGPACK_OBJECT_BIN:
                out_msg->data_sz = obj.via.bin.size;
                out_msg->data = MY_malloc(sizeof(*(out_msg->data))
                                                * out_msg->data_sz);
                memcpy(out_msg->data, obj.via.bin.ptr, out_msg->data_sz);
                break;
            default: break;
        }
    }
    return out_msg;
}

t_node *ra_node_init(void)
{
    t_node *node = 0;

    node = MY_malloc(sizeof(*node));
    node->node_id = 0;
    node->name = 0;
    node->server = 0;
    node->port = 0;
    node->data = 0;
    node->data_sz = 0;
    return node;
}

#define MK_STR_DYN_MEMBER(dst, src, sz)\
   do {\
      (sz) = strlen((src));\
      (dst) = MY_malloc(sizeof(*(dst)) * ((sz) + 1));\
      strncpy((dst), (src), (sz));\
      (dst)[(sz)] = '\0';\
   } while (0)

void ra_node_name(t_node *node, const char* name)
{
    size_t sz = 0;
    MK_STR_DYN_MEMBER(node->name, name, sz);
}

void ra_node_server(t_node *node, const char *server)
{
    size_t sz = 0;
    MK_STR_DYN_MEMBER(node->server, server, sz);
}

#undef MK_STR_DYN_MEMBER

void ra_node_destroy(t_node *node)
{
    MY_free(node->name); node->name = 0;
    MY_free(node->server); node->server = 0;
    MY_free(node->data); node->data = 0;
    MY_free(node); node = 0;
}

t_byte *ra_node_serialize(const t_node *node, size_t *out_sz)
{
    t_byte *buf = 0;
    size_t s_sz = 0;
    msgpack_sbuffer sbuf;
    msgpack_packer pck;

    msgpack_sbuffer_init(&sbuf);
    msgpack_packer_init(&pck, &sbuf, msgpack_sbuffer_write);
    msgpack_pack_array(&pck, 5);
    msgpack_pack_uint64(&pck, node->node_id);
    s_sz = strlen(node->name) + 1;
    msgpack_pack_str(&pck, s_sz); 
    msgpack_pack_str_body(&pck, node->name, s_sz);
    s_sz = strlen(node->server) + 1;
    msgpack_pack_str(&pck, s_sz);
    msgpack_pack_str_body(&pck, node->server, s_sz);
    msgpack_pack_uint16(&pck, node->port);
    msgpack_pack_bin(&pck, node->data_sz);
    msgpack_pack_bin_body(&pck, node->data, node->data_sz);
    *out_sz = sbuf.size;
    buf = MY_malloc(sizeof(*buf) * (*out_sz));
    memcpy(buf, sbuf.data, sbuf.size);
    msgpack_sbuffer_destroy(&sbuf);
    return buf;
}

t_node *ra_node_deserialize(const t_byte *data, size_t data_sz)
{
    msgpack_unpacked msg;
    msgpack_object root;
    t_node *node = 0;
    size_t offset = 0;

    node = ra_node_init();
    msgpack_unpacked_init(&msg);
    if (msgpack_unpack_next(&msg, data, data_sz, &offset)) {
        root = msg.data;
        if (root.type == MSGPACK_OBJECT_ARRAY) {
            node->node_id = root.via.array.ptr[0].via.u64;
            ra_node_name(node, root.via.array.ptr[1].via.str.ptr);
            ra_node_server(node, root.via.array.ptr[2].via.str.ptr);
            node->port = root.via.array.ptr[3].via.u64;
            node->data_sz = root.via.array.ptr[4].via.bin.size;
            if (node->data_sz != 0) {
                node->data = MY_malloc(sizeof(*(node->data)) * node->data_sz);
                memcpy(node->data, root.via.array.ptr[4].via.bin.ptr, node->data_sz);
            }
        }
    }
    msgpack_unpacked_destroy(&msg);
    return node;
}

t_byte *ra_id_serialize(const t_id node_id, size_t *out_sz)
{
    t_byte *buf = 0;
    msgpack_sbuffer sbuf;
    msgpack_packer pck;

    msgpack_sbuffer_init(&sbuf);
    msgpack_packer_init(&pck, &sbuf, msgpack_sbuffer_write);
    msgpack_pack_uint64(&pck, node_id);
    *out_sz = sbuf.size;
    buf = MY_malloc(sizeof(*buf) * (*out_sz));
    memcpy(buf, sbuf.data, sbuf.size);
    msgpack_sbuffer_destroy(&sbuf);
    return buf;
}

t_id ra_id_deserialize(const t_byte *data, size_t data_sz)
{
    msgpack_object obj;
    msgpack_unpacked msg;
    msgpack_unpacked_init(&msg);
    if (msgpack_unpack_next(&msg, data, data_sz, 0)) {
        obj = msg.data;
        if (obj.type == MSGPACK_OBJECT_POSITIVE_INTEGER) {
            return (t_id) obj.via.u64;
        }
    }
    msgpack_unpacked_destroy(&msg);
    return 0;
}

t_byte *ra_str_serialize(const char *str, size_t *out_sz)
{
    t_byte *buf = 0;
    msgpack_sbuffer sbuf;
    msgpack_packer pck;
    size_t sz = 0;

    msgpack_sbuffer_init(&sbuf);
    msgpack_packer_init(&pck, &sbuf, msgpack_sbuffer_write);
    sz = strlen(str);
    msgpack_pack_str(&pck, sz);
    msgpack_pack_str_body(&pck, str, sz);
    *out_sz = sbuf.size;
    buf = MY_malloc(sizeof(*buf) * (*out_sz));
    memcpy(buf, sbuf.data, sbuf.size);
    msgpack_sbuffer_destroy(&sbuf);
    return buf;
}

char *ra_str_deserialize(const t_byte *data, size_t data_sz)
{
    char *rt = 0;
    msgpack_object obj;
    msgpack_unpacked msg;

    msgpack_unpacked_init(&msg);
    if (msgpack_unpack_next(&msg, data, data_sz, 0)) {
        obj = msg.data;
        if (obj.type == MSGPACK_OBJECT_STR) {
            rt = MY_malloc(sizeof(*rt) * (obj.via.str.size + 1));
            strncpy(rt, obj.via.str.ptr, obj.via.str.size);
            rt[obj.via.str.size] = '\0';
        }
    }
    return rt;
}

void ra_g_node_value_destroyer(gpointer data)
{
    ra_node_destroy((t_node *) data);
}

t_master *ra_master_init(void)
{
    t_master *msr = 0;

    msr = MY_malloc(sizeof(*msr));
    msr->master_id = 1;
    msr->slave_id_ctr = msr->master_id + 1;
    msr->node_map = g_hash_table_new_full(g_int_hash,
                    g_int_equal, g_free, ra_g_node_value_destroyer);
    msr->port = PORT;
    return msr;
}

t_id ra_master_next_slave_id(t_master *msr)
{
    /* TODO: use a real generator -- will overflow for
             large numbers of clients */
    return ++(msr->slave_id_ctr);
}

void ra_master_destroy(t_master *msr)
{
    g_hash_table_destroy(msr->node_map); msr->node_map = 0;
    MY_free(msr); msr = 0;
}

void ra_master_proc_register(t_master *msr, int sock, t_message *msg)
{
    t_id slave_id = 0;
    t_node *tmp_node = 0;
    t_byte *data = 0;
    t_message *tmp_msg = 0;
    guint64 *tmp_id = 0;
    size_t data_sz = 0;
    DPRINTF("request to register\n");
    /* accepts a CODE_REGISTER request, generates a new slave ID
       and inserts information about the slave node into the node 
       hashtable */
    /* glib hashtable frees tmp_node */
    tmp_node = ra_node_deserialize(msg->data, msg->data_sz); 
    slave_id = ra_master_next_slave_id(msr);
    tmp_msg = ra_message_init();
    tmp_msg->node_id = slave_id;
    tmp_msg->code = CODE_OK;
    tmp_id = g_malloc(sizeof(*tmp_id)); /* glib hashtable frees */
    tmp_node->node_id = tmp_msg->node_id;
    *tmp_id = tmp_node->node_id; 
    g_hash_table_insert(msr->nodemap, tmp_id, tmp_node);
    data = ra_message_serialize(tmp_msg, &data_sz);
    ra_send_data(sock, data, data_sz);
    ra_message_destroy(tmp_msg); tmp_msg = 0;
    MY_free(data); data = 0;
}

void ra_master_proc_status(t_master *msr, int sock, t_message *msg)
{
    int r_sock = 0, rt = 0;
    t_node *tmp_node = 0;
    t_id r_id = 0;
    t_byte *data = 0;
    char *tmp_str = 0;
    
    /* TODO: deal with errors properly  */
    DPRINTF("request for status of node\n");
    /* client asks for the status of a connected node,
       sends in that remote node ID as part of cur_msg data */
    r_id = ra_id_deserialize(msg->data, msg->data_sz);
    tmp_node = g_hash_table_lookup(msr->node_map, &r_id);
    if (tmp_node == 0) { 
        fprintf(stderr,
            "* * * ERROR: failed processing CODE_STATUS\n");
        return;
    }
    /* node was found, so ask that node for its status */
    tmp_msg = ra_message_init();
    tmp_msg->node_id = msr->master_id;
    tmp_msg->code = CODE_STATUS;
    r_sock = nn_socket(AF_SP, NN_REQ);
    tmp_str = make_message("tcp://%s:%d",
                    tmp_node->server, tmp_node->port);
    rt = nn_connect(r_sock, tmp_str); 
    if (rt == -1) {
        fprintf(stderr,
            "* * * ERROR: failed sending CODE_STATUS to node\n");
    }
    data = ra_message_serialize(tmp_msg, &data_sz);
    ra_send_data(r_sock, data, data_sz);
    MY_free(data); data = 0;
    ra_message_destroy(tmp_msg); tmp_msg;
    MY_free(tmp_str); tmp_str = 0;
    data = ra_recv_data(r_sock, &data_sz);
    tmp_msg = ra_message_deserialize(data, data_sz);
    if (tmp_msg->code == CODE_OK) {
        /* finally send the status back to the original
           requesting client -- sock, not r_sock! */
        tmp_msg->node_id = msr->master_id; /* previous node_id was 
                                              the other node's */
        ra_send_data(sock, tmp_msg->data, tmp_msg->data_sz);
    } else {
        fprintf(stderr,
            "* * * ERROR: failed receiving status from node\n");
    }
    MY_free(data); data = 0;
    MY_free(tmp_msg); tmp_msg = 0;
}

t_message *ra_message_receive(int sock)
{
    t_message *msg = 0;
    t_byte *data = 0;
    size_t data_sz = 0;

    data = ra_recv_data(sock, &data_sz);
    if (data == 0) { return 0; }
    msg = ra_message_deserialize(data, data_sz);
    MY_free(data); data = 0;
    return msg;
}

void *ra_thr_master_server(void *arg)
{
    /* TODO: generate IDs appropriately -- this could overflow  */
    int sock = 0, eid =0; /*, r_sock = 0; */
    t_byte *data = 0;
    t_message *cur_msg = 0, *tmp_msg = 0;
    GHashTable *node_id_map = 0; /* maps t_id to integer */
    size_t data_sz = 0;
    t_id server_node_id = 1;
    uint64_t slave_id_ctr = server_node_id + 1;
    guint64 *tmp_id = 0, *tmp_v = 0;
    t_id r_id = 0;
    t_node *tmp_node = 0;
    char *tmp_str = 0;

    sock = nn_socket(AF_SP, NN_REP);
    eid = nn_bind(sock, "tcp://*:"PORT);
    assert(eid > 0);
    while (1) {
        cur_msg = ra_message_receive(sock);
        if (cur_msg == 0) { continue; }
        if (cur_msg->code != CODE_REGISTER) {
            /* check to see if this server issued the node ID,
               otherwise if not found, ID is invalid, so skip
               the request */
            if (g_hashtable_lookup(msr->hashtable, &(cur_msg->node_id)) != 0) {
                DPRINTF("found ID: %d\n", (int) cur_msg->node_id);
            } else {
                continue;
            }
        }
        switch (cur_msg->code) {
            case CODE_REGISTER:
                DPRINTF("request to register\n");
                ra_master_proc_register(msr, sock, cur_msg);
                break;
            case CODE_STATUS:
                DPRINTF("request for status of node\n");
                ra_master_proc_status(msr, sock, cur_msg);
                break;
            case CODE_OK:
                DPRINTF("acknowledgement of previous request\n");
            default: break;
        }
        ra_message_destroy_with_data(cur_msg); cur_msg = 0;
    }
    nn_close(sock);

    return 0;
}

void *ra_thr_slave_server(void *arg)
{
    int sock = 0, rt = 0;
    char *data = 0;
    char *tmp_str = 0;
    size_t data_sz = 0;
    t_id master_server_id = 0, slave_id = 0;
    lua_State *L = 0;
    t_message *cur_msg = 0, *tmp_msg = 0;
    t_node *node = 0;

    t_master_node_pair

    
    /* TODO: get data from node  passed in */
    
    
    DPRINTF("starting slave server\n");
    L = luaL_newstate();
    lua_register(L, "status", ra_l_status);

    sock = nn_socket(AF_SP, NN_REQ);
    tmp_str = make_message("tcp://%s:%d", "" ); /* FIXME: */
    rt = nn_connect(sock, "tcp://"SERVER":"PORT);
    if (rt == -1) {
        fprintf(stderr, "* * * ERROR: could not connect to server\n");
        return 0; 
    }

    while (1) {
        cur_msg = ra_message_receive(sock);
        if (cur_msg == 0) { continue; }
        switch (cur_msg->code) {
            case CODE_STATUS:
                /* TODO: call through Lua with lua_call */
                tmp_str = ra_status_as_str(); 
                tmp_msg = MY_malloc(sizeof(*tmp_msg));
                tmp_msg->node_id = slave_id;
                tmp_msg->code = CODE_OK;
                tmp_msg->data = ra_str_serialize(tmp_str, &data_sz);
                tmp_msg->data_sz = data_sz;
                data = ra_message_serialize(tmp_msg, &data_sz);
                rt = ra_send_data(sock, data, data_sz);
                if (rt == -1) {
                    fprintf(stderr, "* * * ERROR: could not send status reply\n");
                }
                ra_message_destroy_with_data(tmp_msg); tmp_msg = 0; 
                MY_free(data); data = 0;
                MY_free(tmp_str); tmp_str = 0;
                break;
            default:
                break;
        }
    }

    nn_close(sock);
    lua_close(L);

    return 0;
}

void *ra_thr_client(void *arg)
{
    char *line = 0;
    lua_State *L = 0;
    int rt = 0;

#if 0
    static const luaL_Reg rafflesia_lib[] = {
        {"status", ra_l_status},
        {0, 0}
    };
#endif

    L = luaL_newstate();
    luaL_openlibs(L);
    /*lua_newtable(L);
    luaL_setfuncs(L, rafflesia_lib, 0);
    */
    lua_register(L, "status", ra_l_status);
    printf("Rafflesia\n");
    while (1) {
        line = ra_getline();
        if (line != 0 && strlen(line) > 0) {
            /* TODO: do something */
            rt = luaL_dostring(L, line);
            if (rt != 0) {
                DPRINTF("* * * ERROR: could not execute: %s\n", line);
            }
            /*DPRINTF("ECHO: %s\n", line); */
        }
        MY_free(line); line = 0;
    }
    lua_close(L);

    return 0;
}


void ra_run(const struct arguments *args)
{
    t_thread_info *tinfo = 0;
    pthread_attr_t attr;
    void *res = 0;
    int i = 0, s = 0, n_threads = 2;
    t_node *node = 0;
    t_master *master = 0;
    t_master_slave_pair mspair;

    if (pthread_mutex_init(&S_master_lock, NULL) != 0) {
        fprintf(stderr, "S_master_lock mutex init failed\n");
    }
    assert(VALID_MODE(args->op_mode));
#define CREATE_THREAD(N, THR_FN, DATA)\
        do {\
            tinfo[(N)].thread_num = (N) + 1;\
            tinfo[(N)].data = (DATA);\
            s = pthread_create(&tinfo[(N)].thread_id, &attr,\
                    &((THR_FN)), &tinfo[(N)]); assert(s == 0);\
        } while (0)

    /* S_master_port is needed because of the external call to Lua */
    switch (args->op_mode) {
    case MODE_MASTER: {
#define THR_SERVER 0
#define THR_CLIENT 1
        ra_set_master("localhost", args->local_master_port);  
        master = ra_master_init();
        s = pthread_attr_init(&attr); assert(s == 0);
        tinfo = MY_calloc(n_threads, sizeof(*tinfo));
        CREATE_THREAD(THR_SERVER, ra_thr_master_server, master);
        CREATE_THREAD(THR_CLIENT, ra_thr_client, master);
        s = pthread_attr_destroy(&attr); assert(s == 0);
        for (i = 0; i < n_threads; ++i) {
            s = pthread_join(tinfo[i].thread_id, &res); assert(s == 0);
            MY_free(res); res = 0;
        }
        MY_free(tinfo); tinfo = 0;
        ra_master_destroy(master); master = 0;
        break;
    }
    case MODE_SLAVE: {

        master = ra_master_init();
        node = ra_node_init();
        node->
        mspair.master = master;
        mspair.node = node;


        ra_set_master(args->remote_server, args->remote_port);
        DPRINTF("slave mode\n");
        s = pthread_attr_init(&attr); assert(s == 0);
        tinfo = MY_malloc(sizeof(*tinfo));
        CREATE_THREAD(0, ra_thr_slave_server, 0);
        s = pthread_attr_destroy(&attr); assert(s == 0);
        s = pthread_join(tinfo[0].thread_id, &res); assert(s == 0);

        ra_master_destroy(master); master = 0;
        MY_free(res); res = 0;
        MY_free(tinfo); tinfo = 0;
        break;
    }
    }
    pthread_mutex_destroy(&S_master_lock);
#undef THR_SERVER
#undef THR_CLIENT
#undef CREATE_THREAD
}

