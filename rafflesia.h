/*
 * Copyright © 2015 Izaak 
 *
 * */
#ifndef RAFFLESIA_H
#define RAFFLESIA_H

#include <stdint.h>

enum e_mode {
    MODE_MASTER = 0x1,
    MODE_SLAVE  = 0x2
};
typedef enum e_mode e_mode;

#define VALID_MODE(m) (((m) == MODE_MASTER) || ((m) == MODE_SLAVE))

typedef uint64_t t_id; 
typedef uint64_t t_status_code;
typedef char t_byte;

enum e_status_code {
    CODE_OK                  = 0x1,
    CODE_ERROR               = 0x2,
    CODE_REGISTER            = 0x3,
    CODE_STATUS              = 0x4,
    CODE_EXECUTE             = 0x5
};
typedef enum e_status_code e_status_code;

struct t_message {
    t_id node_id;
    t_status_code code;
    uint64_t data_sz;
    t_byte *data;
};
typedef struct t_message t_message;

struct GHashTable;
typedef struct GHashTable GHashTable;

struct t_master {
    t_id master_id;
    t_id slave_id_ctr;
    GHashTable *node_map;
    uint16_t port;
    char *server;
};
typedef struct t_master t_master;

struct t_node {
    t_id node_id;
    char *name;
    char *local_server;
    uint16_t port;
    size_t data_sz;
    t_byte *data;
};
typedef struct t_node t_node;

struct t_master_slave_pair {
    const t_master *master;
    const t_node *node;
};
typedef struct t_master_slave_pair t_master_slave_pair;

struct arguments {
    e_mode  op_mode;
    uint16_t local_slave_port;
    char *local_slave_address;
    uint16_t local_master_port;
    uint16_t remote_master_port;
    char *remote_master_address;
};

void *MY_malloc(size_t);
void *MY_calloc(size_t, size_t);
void *MY_realloc(void *, size_t);
void MY_free(void *);
void ra_run(const struct arguments *);

#endif

