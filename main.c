#include <argp.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "rafflesia.h"

const char *argp_program_bug_address = "<yitzhak@inbox.ru>";
const char *argp_program_version = "rafflesia-0.01";
static char doc[] = "rafflesia documentation";
static struct argp_option options[] = {
    {"mode", 'a', "MODE", 0, "Specifies a mode. Valid modes: master, slave.", 0},
    {"local-master-port", 'b', "LOCAL-MASTER-PORT", 0, "Specifies port of local master server. Only applicable in master mode", 0},
    {"remote-master-port", 'c', "REMOTE-MASTER-PORT", 0, "Port of remote server master. Only applicable in slave mode.", 0},
    {"remote-master-address", 'd', "REMOTE-MASTER-ADDRESS", 0, "Address of remote server. Only applicable in slave mode.", 0},
    {"local-slave-port", 'e', "LOCAL-SLAVE-PORT", 0, "Specifies port of local slave server. Only applicable in slave mode.", 0},
    {"local-slave-address", 'f', "LOCAL-SLAVE-ADDRESS",  0, "Specifies IP address of local slave server. Only applicable in slave mode."},
    { 0, 0, 0, 0, 0, 0 } 
};

static error_t parse_opt (int key, char *arg, struct argp_state *state)
{
    struct arguments *arguments = state->input;
    static int master_flag = 0,
               slave_flag = 0,
               local_master_port_flag = 0,
               remote_master_port_flag = 0,
               remote_master_address_flag = 0,
               local_slave_port_flag = 0,
               local_slave_address_flag = 0;
   size_t sz = 0;

#define STR_ARG_NUM(x, y)\
            do {\
                n = strtol((x), 0, 10);\
                if (n == LONG_MIN || n == LONG_MAX) {\
                    fprintf(stderr, "* * * ERROR: number invalid for %s", #y);\
                }\
                y = n;\
            } while (0)

#define STR_ARG_CPY(x, y, ln)\
                do {\
                    ln = strlen((x));\
                    y = MY_malloc(sizeof(*(y)) * ((ln) + 1));\
                    strncpy((y), (x), (ln));\
                    y[(ln)] = '\0';\
                } while (0)
    switch (key) { 
    case 'a':
        sz = strlen(arg);
        if (strncmp(arg, "master", 6) == 0) {
            arguments->op_mode = MODE_MASTER;
            master_flag = 1;
        else if (strncmp(arg, "slave", 5) == 0) {
            arguments->op_mode = MODE_SLAVE;
            slave_flag = 1;
        }
        break;
    case 'b':
        STR_ARG_NUM(arg, arguments->local_master_port);
        local_master_port_flag = 1;
        break;
    case 'c':
        STR_ARG_NUM(arg, arguments->remote_master_port);
        remote_master_port_flag = 1;
        break;
    case 'd':
        STR_ARG_CPY(arg, arguments->remote_master_address, sz);
        remote_master_address_flag = 1;
        break;
    case 'e':
        STR_ARG_NUM(arg, arguments->local_slave_port);
        local_slave_port_flag = 1; 
        break;
    case 'f':
        STR_ARG_CPY(arg, arguments->local_slave_address, sz);
        local_slave_address_flag = 1;
        break;
    case ARGP_KEY_END:
        /* all of the invalid combinations of options */
        /* there's probably a better way... */
        if ( (master_flag == 1 && local_slave_port_flag == 1) ||   
             (master_flag == 1 && local_slave_address_flag == 1) ||
             (master_flag == 1 && remote_master_port_flag == 1) ||
             (master_flag == 1 && remote_master_address_flag == 1)) ||
             (slave_flag == 1 && local_master_port_flag == 1) ||
             (master_flag == 1 && slave_flag == 1) ||
             (master_flag == 0 && slave_flag == 0)
        {
            argp_usage(state);
        }
        break;
    default: return ARGP_ERR_UNKNOWN;
    }
#undef STR_ARG_CPY
#undef STR_ARG_NUM
    return 0;
}

static struct argp argp = { options, parse_opt, 0, doc, 0, 0, 0 };

int main(int argc, char *argv[])
{
    struct arguments args;
 
    /* default to master mode */
    args.mode = MODE_MASTER;
    args.local_slave_port = 0;
    args.local_master_port = 4567;
    args.remote_port = 0;
    args.remote_server = 0;
    argp_parse(&argp, argc, argv, 0, 0, &args);
    if (VALID_MODE(args.mode)) {
        ra_run(&args);
    }
    MY_free(args.local_slave_port); args.local_slave_port = 0;
    MY_free(args.local_master_port); args.local_master_port = 0;
    MY_free(args.remote_port); args.remote_port = 0;
    MY_free(args.remote_server); args.remote_server = 0;
    
    return 0;
}

